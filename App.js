
import React, { Component } from 'react'
import {View,Text} from 'react-native'
import {createAppContainer} from 'react-navigation'
import {createMaterialTopTabNavigator} from 'react-navigation-tabs'
import {createStackNavigator} from 'react-navigation-stack'
import FirstTab from './Tabs/First'
import SecondTab from './Tabs/Second'
import ThirdTab from './Tabs/Third'

const TabScreen=createMaterialTopTabNavigator({
  First:{
    screen:FirstTab
  },
  Second:{
    screen:SecondTab
  },
  Third:{
    screen:ThirdTab
  }
},
{
  tabBarPosition: 'top',
  swipeEnabled:true,
  //animationEnabled: true,
  tabBarOptions: {
    activeTintColor: '#C84080',
    inactiveTintColor: '#9FA0A3',
    style: {
      backgroundColor: '#E8E8E8',
    },
    labelStyle: {
      textAlign: 'center',
    },
    indicatorStyle: {
      borderBottomColor: '#C84080',
      borderBottomWidth: 3,
    },
  },
}
)
const App=createStackNavigator({
  TabScreen:{
    screen:TabScreen,
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#C84080',
      },
      headerTintColor: '#FFFFFF',
      title: 'TabExample',
      
    },
  }
},{headerLayoutPreset: 'center'})
export default createAppContainer(App)

